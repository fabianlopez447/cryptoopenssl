Proyecto para encriptar y desencriptar en BASE64.


Funciones:

sslPrm: Se configuran las claves bajo las cuales se desea encriptar y desencriptar, las misma deben ser únicas y difíciles de descifrar.


sslEnc: función para encryptar, Recibe el texto y con los parámetro configurados en el método sslPrm devuelve el texto encriptado.


sslDec: la misma recibe texto encriptado bajo las misma claves configuradas en el método sslPrm y lo devulve de manera legible.

