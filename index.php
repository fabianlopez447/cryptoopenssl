
<?php

function sslPrm()
{
  //Se configura la clave 1, clave 2 y el nivel y método de encriptación 
return array('clave1','clave2','aes-128-cbc');
}
function sslEnc($msg)
{
  list ($pass, $iv, $method)=sslPrm();
  if(function_exists('openssl_encrypt'))
     return openssl_encrypt($msg, $method, $pass, false, $iv);
  else
     return exec('echo \''.$msg.'\' | openssl enc -'.urlencode($method).' -base64 -nosalt -K '.bin2hex($pass).' -iv '.bin2hex($iv));
}
function sslDec($msg)
{
  list ($pass, $iv, $method)=sslPrm();
  if(function_exists('openssl_decrypt'))
     return trim(openssl_decrypt($msg, $method, $pass, false, $iv));
  else
     return trim(exec('echo \''.$msg.'\' | openssl enc -'.$method.' -d -base64 -nosalt -K '.bin2hex($pass).' -iv '.bin2hex($iv)));
}


$encriptar = sslEnc("Hola Mundo");


$desencriptar = sslDec("AQUÍ TEXTO ENCRIPTADO");

?>


<!DOCTYPE html>
<html>
<head>
  <title></title>
  <style>
  .row {
  display: flex;
  padding: 100px;
}

.column {
  flex: 50%;
  margin: 10px;
}
  </style>
</head>
<body>

<div class="row">
  <div class="column" style="border: solid 1px red;">
    <h3 style="text-align: center; color: red;">Encriptado</h3>
    <div style="padding: 10px;max-width: 100%;">
      <textarea style=" width: 100%;" name="" id="" cols="30" rows="10"> <?php echo ($encriptar); ?></textarea>
    </div>
  </div>
  <div class="column" style="border: solid 1px blue;">
    <h3 style="text-align: center; color: blue;">Desencriptado</h3>
    <div style="padding: 10px;max-width: 100%;">
      <!-- <code>
          <?php echo ($desencriptar); ?>
      </code> -->
      <textarea style=" width: 100%;" name="" id="" cols="30" rows="10"> <?php echo ($desencriptar); ?> </textarea>
    </div>
    
  </div>
</div>

</body>
</html>
